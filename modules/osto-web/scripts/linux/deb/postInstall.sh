#!/usr/bin/env bash

echo "postInstall"
echo $1

echo "Setting permission's"
chmod -R o-rwx /opt/osto-web
chmod -R u+x /opt/osto-web/bin/*.sh

echo "Activating systemd"
systemctl enable osto-web.service