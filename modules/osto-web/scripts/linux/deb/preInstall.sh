#!/usr/bin/env bash

echo "preInstall"
echo $1

echo "Creating group: osto"
/usr/sbin/groupadd -f -r osto 2> /dev/null || true

echo "Creating user:osto"
/usr/sbin/useradd osto -r -s /sbin/nologin -g osto 2> /dev/null || true