#!/bin/bash

# BASIC STUFF
export PID_FILE=$APPLICATION_HOME/osto-web.pid

# JAVA_HOME TO USE
export JAVACMD="java"
export JAVA_MAIN_CLASS=biz.db.dev.osto.web.RootOstoWeb
export APPLICATION_MARKER=osto-web

DEFAULT_JVM_OPTS=" -Dapp.name=$APPLICATION_MARKER "
DEFAULT_JVM_OPTS="$DEFAULT_JVM_OPTS  -Xms128M -Xmx256M "

DEFAULT_JVM_OPTS="$DEFAULT_JVM_OPTS -Dspring.profiles.active=debian -Dspring.config.location=$APPLICATION_CONFIG/application.yml -Dlogging.config=$APPLICATION_CONFIG/logback.xml -Dlogging.path=$APPLICATION_LOGS"

export DEFAULT_JVM_OPTS;