#!/bin/bash

export APPLICATION_HOME=/opt/osto-web
export APPLICATION_CONFIG=/etc/osto-web
export APPLICATION_LOGS=/var/log/osto-web
export APPLICATION_EXTERNAL_CONFIG=$APPLICATION_HOME/bin/setconfig.sh


#if [ ! -f "$APPLICATION_EXTERNAL_CONFIG" ]; then
#    echo "Could not find external config file. Refusing to start"
#    exit 1;
#fi
#source $APPLICATION_EXTERNAL_CONFIG

source $APPLICATION_HOME/bin/setenv.sh

# move to home
cd "$APPLICATION_HOME"

# build classpath
CLASSPATH=""
FIRST=0

for jar in lib/*.jar
do
        if [ "$FIRST" -eq "0" ]
        then
                CLASSPATH=$jar
                FIRST=1
        else
                CLASSPATH=$CLASSPATH:$jar
        fi
done

# build final command
COMMAND="$JAVACMD $DEFAULT_JVM_OPTS -classpath $CLASSPATH $JAVA_MAIN_CLASS"

if [ "$1" = "start" ] ; then

        if [ -f "$PID_FILE" ]; then
                echo "pid file found at $PID_FILE. Did you already started the application?"
                exit 1;
        fi

        nohup $COMMAND > /dev/null 2>&1 &

        # wait for boot
        sleep 10

        # get running pid
        PID=$(cat $PID_FILE)

        echo "Started application with process PID: $PID"
        exit 0;

elif [ "$1" = "stop" ]; then

        # check pid
        if [ ! -f "$PID_FILE" ]; then
                echo "pid file not found at $PID_FILE. Did you started the program first?"
                exit 1;
        fi

        # Get PID of running process
        PID=$(cat $PID_FILE)

        # check if this process is still active
        RESULT=$(ps -ef | grep "app.name=$APPLICATION_MARKER" | grep $PID)
        if [ "${#RESULT}" -eq 0 ]; then
                echo "Application process not found with PID: $PID. Maybe it already terminated?";
        else
                kill $PID
        fi;

        exit 0;
elif [ "$1" = "status" ]; then

        if [ ! -f "$PID_FILE" ]; then
                echo "Application is not running";
                exit 0;
        fi

        # Get PID of running process
        PID=$(cat $PID_FILE);

        # check if this process is still active
        RESULT=$(ps -ef | grep "app.name=$APPLICATION_MARKER" | grep $PID);
        if [ "${#RESULT}" -eq 0 ]; then
                echo "Application is not running";
        else
                echo "Application is running"
        fi;
        exit 0;
else
    echo "Invalid command. Usage $0 start|stop|status";
fi

exit 0;