package biz.db.dev.osto.web.validations;

import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

public class ValidationsTest {

    @Test
    public void testTeamPatterns() {
        Pattern pattern = Pattern.compile(MavenPatterns.TEAM_PATTERN);

        assertThat(pattern.matcher("kjurasovic").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("kjurasovic21").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("21kjurasovic").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("21team21").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("team_21").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("_team_21").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("team-21").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("team.21").matches(), is(equalTo(false)));
    }

    @Test
    public void testRepositoryNamePatterns() {
        Pattern pattern = Pattern.compile(MavenPatterns.REPOSITORY_NAME_PATTERN);

        assertThat(pattern.matcher("build").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("team-build").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("team21").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("21team").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("21team21").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("team21team").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("team-21-team").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("team.21.team").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("team_21_team").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("-team-21-team").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("-team-21-team-").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("team-21-team-").matches(), is(equalTo(false)));
    }

    @Test
    public void testGroupIdPatterns() {
        Pattern pattern = Pattern.compile(MavenPatterns.GROUP_ID_PATTERN);

        assertThat(pattern.matcher("biz.db.dev.osto").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit5").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit51").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit.").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("2junit").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("junit-51").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit-5").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit_51").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit_5").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("_junit").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("junit_").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("-junit").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("junit-").matches(), is(equalTo(false)));

        assertThat(pattern.matcher("org.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org2.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org-2.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org_2.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org21.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org-21.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org_21.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.demo2").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.demo21").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.demo-2").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.demo_2").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.demo-21").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.demo_21").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.company.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org2.company.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org21.company.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.company2.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.company21.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.company.demo2").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.company.demo21").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org12.company34.demo56").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org.demo.").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("org.demo.company.").matches(), is(equalTo(false)));

        assertThat(pattern.matcher("org.company-main.demo").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("org..company.demo").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("org.company..demo").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("org.company.demo.").matches(), is(equalTo(false)));
    }

    @Test
    public void testArtifactIdPatterns() {
        Pattern pattern = Pattern.compile(MavenPatterns.ARTIFACT_ID_PATTERN);

        // every combination of letters, numbers , _ , -, .

        assertThat(pattern.matcher("junit-core").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit-core-2.1").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit-core_2.1").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("atto-core_2.11").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("sangria_2.11").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("jboss-ejb-client").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("akka-actor_2.11").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("voodoo_2.11").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("case-app_2.11").matches(), is(equalTo(true)));

        assertThat(pattern.matcher("junit-core ").matches(), is(equalTo(false)));
        assertThat(pattern.matcher(" junit-core").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("junit-core&").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("junit-core\\").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("junit-core/").matches(), is(equalTo(false)));
    }

    @Test
    public void testVersionPatterns() {
        Pattern pattern = Pattern.compile(MavenPatterns.VERSION_ID_PATTERN);

        // every combination of letters, numbers , _ , -, .

        assertThat(pattern.matcher("1").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("1.0").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("1.0.13").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("5.0.1.RELEASE").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("5.0.1.SNAPSHOT").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("5.0.1_RELEASE").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("5.0.1_SNAPSHOT").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("5.0.RELEASE").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("5.0.SNAPSHOT").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("5.RELEASE").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("5.SNAPSHOT").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("1.0.13.redhat-4").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("5.2.12.Final").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("1.0.4-patch9").matches(), is(equalTo(true)));


        assertThat(pattern.matcher("1 ").matches(), is(equalTo(false)));
        assertThat(pattern.matcher(" 1").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("1/").matches(), is(equalTo(false)));
        assertThat(pattern.matcher("1\\").matches(), is(equalTo(false)));
    }

    @Test
    public void testFilenamePatterns() {
        Pattern pattern = Pattern.compile(MavenPatterns.FILENAME_PATTERN);

        assertThat(pattern.matcher("junit-1.0.jar").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit-1.0.pom").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit-1.0.jar.md5").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("junit-1.0.jar.sha1").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("db-ojb-1.0.4-patch9.jar").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("dotty-interfaces-0.5.0-bin-20171031-3a72da6-NIGHTLY.jar").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("sangria_2.11-1.3.2.jar").matches(), is(equalTo(true)));
        assertThat(pattern.matcher("atto-core_2.12-0.6.1-M6.jar").matches(), is(equalTo(true)));

        assertThat(pattern.matcher("junit-1.0.jar.").matches(), is(equalTo(false)));
    }
}
