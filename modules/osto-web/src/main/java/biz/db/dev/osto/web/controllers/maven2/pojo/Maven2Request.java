package biz.db.dev.osto.web.controllers.maven2.pojo;

import biz.db.dev.osto.web.validations.ArtifactIdConstraint;
import biz.db.dev.osto.web.validations.FilenameConstraint;
import biz.db.dev.osto.web.validations.GroupIdConstraint;
import biz.db.dev.osto.web.validations.RepositoryNameConstraint;
import biz.db.dev.osto.web.validations.TeamConstraint;
import biz.db.dev.osto.web.validations.VersionConstraint;

public class Maven2Request {

    @TeamConstraint
    private String team;

    @RepositoryNameConstraint
    private String repositoryName;

    @GroupIdConstraint
    private String groupId;

    @ArtifactIdConstraint
    private String artifactId;

    @VersionConstraint
    private String version;

    @FilenameConstraint
    private String filename;

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public String toString() {
        return "Maven2Request{" +
                "team='" + team + '\'' +
                ", groupId='" + groupId + '\'' +
                ", artifactId='" + artifactId + '\'' +
                ", version='" + version + '\'' +
                ", filename='" + filename + '\'' +
                '}';
    }
}
