package biz.db.dev.osto.web.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(value = -101) // spring security filter is -100
public class RewriteFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(RewriteFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        if( logger.isDebugEnabled()) {
            logger.debug("doFilterInternal RewriteFilter");
        }

        RepositoryBuilder builder = new RepositoryBuilder(request);

        if(builder.isValid()) {

            if( logger.isDebugEnabled()) {
                logger.debug("forwarding to url {}", builder.getNewUrl());
            }

            request.getRequestDispatcher(builder.getNewUrl()).forward(request, response);
        } else {
            filterChain.doFilter(request, response);
        }
    }
}
