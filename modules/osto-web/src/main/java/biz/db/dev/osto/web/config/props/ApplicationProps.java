package biz.db.dev.osto.web.config.props;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties( prefix = "osto")
public class ApplicationProps {

    private String repositoryRoot;

    private String repositoryMasterUsername;

    private String repositoryMasterPassword;

    public String getRepositoryRoot() {
        return repositoryRoot;
    }

    public void setRepositoryRoot(String repositoryRoot) {
        this.repositoryRoot = repositoryRoot;
    }

    public String getRepositoryMasterUsername() {
        return repositoryMasterUsername;
    }

    public void setRepositoryMasterUsername(String repositoryMasterUsername) {
        this.repositoryMasterUsername = repositoryMasterUsername;
    }

    public String getRepositoryMasterPassword() {
        return repositoryMasterPassword;
    }

    public void setRepositoryMasterPassword(String repositoryMasterPassword) {
        this.repositoryMasterPassword = repositoryMasterPassword;
    }
}
