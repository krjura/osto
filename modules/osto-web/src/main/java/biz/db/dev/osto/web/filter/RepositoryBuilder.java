package biz.db.dev.osto.web.filter;

import javax.servlet.http.HttpServletRequest;

public class RepositoryBuilder {

    private static final String EMPTY_CONTEXT_PATH = "/";
    private static final String REPOSITORY_TAG = "/repository";
    private static final String PATH_SPLITTER = "/";
    private static final String SEPARATOR_DOT = ".";

    private String type;
    private String team;
    private String repositoryName;
    private String groupId;
    private String artifactId;
    private String version;
    private String filename;

    private String newUrl;

    private boolean isValid;

    public RepositoryBuilder(HttpServletRequest request) {
        processUri(request);
        calculateRealPath();
    }

    public String getType() {
        return type;
    }

    public String getTeam() {
        return team;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public String getVersion() {
        return version;
    }

    public String getFilename() {
        return filename;
    }

    public String getNewUrl() {
        return newUrl;
    }

    public boolean isValid() {
        return isValid;
    }

    private void calculateRealPath() {
        this.newUrl = REPOSITORY_TAG + "/" + this.type +
                "?team=" + this.team +
                "&repositoryName=" + this.repositoryName +
                "&groupId=" + this.groupId +
                "&artifactId=" + this.artifactId +
                "&version=" + this.version +
                "&filename=" + this.filename;
    }

    private void processUri(HttpServletRequest request) {
        String uri = trimContextPath(request);

        if( !uri.startsWith(REPOSITORY_TAG)) {
            this.isValid = false;
            return;
        }

        String[] paths = uri.split(PATH_SPLITTER);

        // /repository/<type>/<team>/<repository name>/<groupId>/<artifactId>/<version>/<filename>
        if(paths.length < 7) {
            this.isValid = false;
            return;
        }

        processPaths(paths);
    }

    private void processPaths(String[] paths) {

        int typeIndex = 2;
        int teamIndex = 3;
        int repositoryNameIndex = 4;
        int filenameIndex = paths.length - 1;
        int versionIndex = paths.length - 2;
        int artifactIdIndex = paths.length - 3;

        this.type = paths[typeIndex];
        this.team = paths[teamIndex];
        this.repositoryName = paths[repositoryNameIndex];
        this.groupId = groupId(paths, repositoryNameIndex + 1, artifactIdIndex);
        this.artifactId = paths[artifactIdIndex];
        this.version = paths[versionIndex];
        this.filename = paths[filenameIndex];

        this.isValid = true;
    }

    private String trimContextPath(HttpServletRequest request) {
        String uri = request.getRequestURI();
        String contextPath = request.getContextPath();

        if(EMPTY_CONTEXT_PATH.equals(contextPath)) {
            return uri;
        }

        return uri.substring(contextPath.length());
    }

    private String groupId(String[] paths, int startIndex, int endIndex) {
        StringBuilder builder = new StringBuilder();

        for( int i = startIndex; i < endIndex; i++) {
            builder.append(PATH_SPLITTER).append(paths[i]);
        }

        // replace / with .
        // remove first /
        return builder.toString().replaceAll(PATH_SPLITTER, SEPARATOR_DOT).substring(1);
    }
}
