package biz.db.dev.osto.web.services.pojo;

public class Maven2ServiceRequest {

    private String team;

    private String repositoryName;

    private String groupId;

    private String artifactId;

    private String version;

    private String filename;

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public Maven2ServiceRequest team(String team) {
        this.team = team;

        return this;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public Maven2ServiceRequest repositoryName(String repositoryName) {
        this.repositoryName = repositoryName;

        return this;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Maven2ServiceRequest groupId(String groupId) {
        this.groupId = groupId;

        return this;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public Maven2ServiceRequest artifactId(String artifactId) {
        this.artifactId = artifactId;

        return this;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Maven2ServiceRequest version(String version) {
        this.version = version;

        return this;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Maven2ServiceRequest filename(String filename) {
        this.filename = filename;

        return this;
    }
}
