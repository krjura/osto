package biz.db.dev.osto.web.config;

import biz.db.dev.osto.web.config.props.ApplicationProps;
import biz.db.dev.osto.web.enums.Grants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/**").hasAuthority(Grants.ROLE_ADMIN.name())
                .and()
                .httpBasic();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);
        http.csrf().disable();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth, ApplicationProps props) throws Exception {
        auth
                .inMemoryAuthentication()
                .withUser(props.getRepositoryMasterUsername())
                .password(props.getRepositoryMasterPassword())
                .authorities(Grants.ROLE_ADMIN.name());
    }
}