package biz.db.dev.osto.web.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = { })
@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@Pattern(regexp = MavenPatterns.VERSION_ID_PATTERN)
@NotNull
public @interface VersionConstraint {

    String message() default "osto.VersionConstraint.message";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
