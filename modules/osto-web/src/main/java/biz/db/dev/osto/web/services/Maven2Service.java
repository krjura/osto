package biz.db.dev.osto.web.services;

import biz.db.dev.osto.web.ex.ServiceException;
import biz.db.dev.osto.web.services.pojo.Maven2ServiceRequest;

import java.io.File;
import java.io.InputStream;
import java.util.Optional;

public interface Maven2Service {

    Optional<File> downloadMavenArtifact(Maven2ServiceRequest request);

    void uploadMavenArtifact(Maven2ServiceRequest request, InputStream is) throws ServiceException;

    boolean shouldBeIgnored(String filename);
}
