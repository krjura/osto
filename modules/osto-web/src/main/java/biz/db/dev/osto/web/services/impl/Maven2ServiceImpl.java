package biz.db.dev.osto.web.services.impl;

import biz.db.dev.osto.web.config.props.ApplicationProps;
import biz.db.dev.osto.web.ex.ServiceException;
import biz.db.dev.osto.web.services.Maven2Service;
import biz.db.dev.osto.web.services.pojo.Maven2ServiceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class Maven2ServiceImpl implements Maven2Service {

    private static final Logger logger = LoggerFactory.getLogger(Maven2ServiceImpl.class);

    private static final String DOT_SEPARATOR = ".";
    private static final String REPOSITORY_TYPE_MAVEN2 = "maven2";

    private static final List<String> FILENAME_IGNORE_LIST = Arrays.asList(
            "maven-metadata.xml", "maven-metadata.xml.sha1", "maven-metadata.xml.md5");

    private final ApplicationProps applicationProps;

    public Maven2ServiceImpl(ApplicationProps applicationProps) {
        Assert.notNull(applicationProps, "ApplicationProps cannot be null");

        this.applicationProps = applicationProps;
    }

    @Override
    public Optional<File> downloadMavenArtifact(Maven2ServiceRequest request) {

        String groupIdAsPath = buildGroupIdPath(request.getGroupId());
        String resourceFile = buildResourceFile(request, groupIdAsPath);

        if(logger.isDebugEnabled()) {
            logger.debug("filename path is {}", resourceFile);
        }

        File filename = new File(resourceFile);

        if( !filename.exists() ) {
            return Optional.empty();
        }

        return Optional.of(filename);
    }

    @Override
    public void uploadMavenArtifact(Maven2ServiceRequest request, InputStream is) throws ServiceException {
        String groupIdAsPath = buildGroupIdPath(request.getGroupId());
        String resourceDirectory = buildResourceDirectory(request, groupIdAsPath);
        String resourceFile = resourceDirectory + File.separator + request.getFilename();

        try {
            // create directories if they do not exist
            Files.createDirectories(Paths.get(resourceDirectory));

            if (logger.isDebugEnabled()) {
                logger.debug("filename path is {}", resourceFile);
            }

            File filename = new File(resourceFile);

            try (FileOutputStream requestFileStream = new FileOutputStream(filename, true)) {
                FileCopyUtils.copy(is, requestFileStream);
            }
        } catch (IOException e) {
            logger.warn("cannot upload file", e);
            throw new ServiceException("cannot upload file", e);
        }
    }

    @Override
    public boolean shouldBeIgnored(String filename) {
        if(FILENAME_IGNORE_LIST.contains(filename)) {

            if( logger.isDebugEnabled()) {
                logger.debug("Ignoring {}", filename);
            }

            return true;
        }

        return false;
    }

    private String buildResourceFile(Maven2ServiceRequest request, String groupIdAsPath) {
        return buildResourceDirectory(request, groupIdAsPath) +
                File.separator +
                request.getFilename();
    }

    private String buildResourceDirectory(Maven2ServiceRequest request, String groupIdAsPath) {
        return applicationProps.getRepositoryRoot() + File.separator +
                REPOSITORY_TYPE_MAVEN2 + File.separator +
                request.getTeam() + File.separator +
                request.getRepositoryName() + File.separator +
                groupIdAsPath + File.separator +
                request.getArtifactId() + File.separator +
                request.getVersion();
    }

    private String buildGroupIdPath(String groupId) {
        return groupId.replace(DOT_SEPARATOR, File.separator);
    }
}
