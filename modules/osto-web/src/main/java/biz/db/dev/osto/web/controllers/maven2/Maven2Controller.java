package biz.db.dev.osto.web.controllers.maven2;

import biz.db.dev.osto.web.controllers.maven2.pojo.Maven2Request;
import biz.db.dev.osto.web.ex.ServiceException;
import biz.db.dev.osto.web.services.Maven2Service;
import biz.db.dev.osto.web.services.pojo.Maven2ServiceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

@Controller
public class Maven2Controller {

    private static final Logger logger = LoggerFactory.getLogger(Maven2Controller.class);

    private static final String EXTENSION_POM = ".pom";
    private static final String EXTENSION_JAR = ".jar";
    private static final String JAR_MEDIA_TYPE = "application/java-archive";

    private Maven2Service maven2Service;

    public Maven2Controller(Maven2Service maven2Service) {
        Assert.notNull(maven2Service, "Maven2Service cannot be null");

        this.maven2Service = maven2Service;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            path = "/repository/maven2"
    )
    @ResponseBody
    public ResponseEntity<Resource> maven2Download(@Valid Maven2Request request) {

        Maven2ServiceRequest serviceRequest = new Maven2ServiceRequest()
                .team(request.getTeam())
                .repositoryName(request.getRepositoryName())
                .groupId(request.getGroupId())
                .artifactId(request.getArtifactId())
                .version(request.getVersion())
                .filename(request.getFilename());

        Optional<File> optionalRequestFile = maven2Service.downloadMavenArtifact(serviceRequest);

        if( !optionalRequestFile.isPresent() ) {
            return ResponseEntity.notFound().build();
        }

        FileSystemResource resource = new FileSystemResource(optionalRequestFile.get());

        return ResponseEntity.ok()
                .contentType(resolveContentType(request.getFilename()))
                .body(resource);
    }

    @RequestMapping(
            method = RequestMethod.PUT,
            path = "/repository/maven2"
    )
    @ResponseBody
    public ResponseEntity<Void> maven2Upload(@Valid Maven2Request request, HttpServletRequest servletRequest) {

        if(this.maven2Service.shouldBeIgnored(request.getFilename())) {
            return ResponseEntity.ok().build();
        }

        Maven2ServiceRequest serviceRequest = new Maven2ServiceRequest()
                .team(request.getTeam())
                .repositoryName(request.getRepositoryName())
                .groupId(request.getGroupId())
                .artifactId(request.getArtifactId())
                .version(request.getVersion())
                .filename(request.getFilename());

        try {
            this.maven2Service.uploadMavenArtifact(serviceRequest, servletRequest.getInputStream());

            return ResponseEntity.ok().build();
        } catch (ServiceException | IOException e) {
            logger.warn("Cannot upload maven artifact", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private MediaType resolveContentType(String filename) {
        String filenameLowerCase = filename.toLowerCase();

        if(filenameLowerCase.endsWith(EXTENSION_POM)) {
            return MediaType.TEXT_XML;
        } else if(filenameLowerCase.endsWith(EXTENSION_JAR)) {
            return MediaType.parseMediaType(JAR_MEDIA_TYPE);
        } else {
            return MediaType.APPLICATION_OCTET_STREAM;
        }
    }
}
