package biz.db.dev.osto.web.validations;

public class MavenPatterns {

    public static final String TEAM_PATTERN = "[a-z0-9]+";
    public static final String REPOSITORY_NAME_PATTERN = "[a-z0-9]+[a-z0-9-]+[a-z0-9]+";
    public static final String GROUP_ID_PATTERN = "([a-z]+[a-z0-9_-]*[a-z0-9]+)|(([a-z]+[a-z0-9_-]*[a-z0-9]+)\\.)+([a-z]+[a-z0-9_-]*[a-z0-9]+)";
    public static final String ARTIFACT_ID_PATTERN = "([a-z0-9\\_\\-\\.])+";
    public static final String VERSION_ID_PATTERN = "([a-zA-Z0-9\\_\\-\\.])+";
    public static final String FILENAME_PATTERN = ARTIFACT_ID_PATTERN + "-" + VERSION_ID_PATTERN + "(\\.[a-z0-9]+)+";
}
