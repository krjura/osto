package biz.db.dev.osto.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan( basePackageClasses = {
        RootOstoWeb.class
})
public class RootOstoWeb {

    private void runSpring(String[] args) {
        SpringApplication springApplication = new SpringApplication(RootOstoWeb.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);
    }

    public static void main(String[] args) {
        RootOstoWeb application = new RootOstoWeb();
        application.runSpring(args);
    }
}
